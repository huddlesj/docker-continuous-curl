FROM alpine:3.9

RUN apk add --update \
    tini \
    curl \
    && rm -rf /var/cache/apk/*

#--repository https://dl-cdn.alpinelinux.org/alpine/edge/community/ \

COPY entrypoint.sh /entrypoint.sh

ENTRYPOINT ["/sbin/tini", "--", "/entrypoint.sh"]

